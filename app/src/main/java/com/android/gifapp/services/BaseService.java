package com.android.gifapp.services;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Chakravarty G on 12/06/2016.
 */
public class BaseService {

    protected static final String JSON_CONTENT_TYPE = "application/json";
    protected static final String BASE_URL = "";

    //protected static final MediaType JSON_MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");
    protected static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    protected static final String AUTHORIZATION_HEADER_VALUE = "Bearer ";
    private static final int AUTHORIZATION_FAILURE_HTTP_CODE = 401;
    private static final int INTERNAL_SERVER_ERROR_HTTP_CODE = 500;
    private static final int DEFAULT_TIMEOUT = 30 * 10000;

    protected static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    public static OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    protected static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();
    protected static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();


    public static boolean isJSONValid(String json) {
        try {
            if (json.startsWith("[")) {
                new JSONArray(json);
                return true;
            } else {
                new JSONObject(json);
                return true;
            }

        } catch (JSONException ex) {

            return false;

        }
    }


}
